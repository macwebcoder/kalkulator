(function () {

    var calculator = {
        calcValueX: null,
        calcValueY: null,
        calcOperation: null,
        isDisplayValueSet: false,
        resetCalc: function () {
            this.calcValueX = null;
            this.calcValueY = null;
            this.calcOperation = null;
        }
    };

    resetDisplay();

    function resetDisplay() {
        updateDisplay(0);
    }

    function addValues(val1, val2) {
        return val1 + val2;
    }

    function substractValues(val1, val2) {
        return val1 - val2;
    }

    function multiplyValues(val1, val2) {
        return val1 * val2;
    }

    function divideValues(val1, val2) {
        if (val2 === 0) {
            return 'Błąd!';
        }

        return val1 / val2;
    }

    function updateDisplay(value) {
        var display = document.getElementById('calcDisplay');
        display.textContent = value;
    }

    function getDisplayValue() {
        return document.getElementById('calcDisplay').textContent;
    }

    function numberClick(value) {
        var displayValue = getDisplayValue();

        if (calculator.isDisplayValueSet) {
            displayValue = '';
            calculator.isDisplayValueSet = false;
        }

        if (displayValue === '0') {
            displayValue = '';
        }

        value = displayValue + value;
        updateDisplay(value);
    }

    function operationClick(operation) {
        if (operation === 'RESET') {
            calculator.resetCalc();
            resetDisplay();
            return;
        }

        if (!calculator.calcValueX) {
            calculator.calcValueX = parseFloat(getDisplayValue());
        } else {
            calculator.calcValueY = parseFloat(getDisplayValue());
        }

        if (calculator.calcOperation) {
            calculate();
        }

        if (operation === 'RESULT') {
            calculator.resetCalc();
        } else {
            calculator.calcOperation = operation;
        }

        calculator.isDisplayValueSet = true;
    }

    function calculate() {
        var calcResult = 0;
        switch (calculator.calcOperation) {
            case 'ADD':
                calcResult = addValues(calculator.calcValueX, calculator.calcValueY);
                break;

            case 'SUB':
                calcResult = substractValues(calculator.calcValueX, calculator.calcValueY);
                break;

            case 'MULTIPLY':
                calcResult = multiplyValues(calculator.calcValueX, calculator.calcValueY);
                break;

            case 'DIVIDE':
                calcResult = divideValues(calculator.calcValueX, calculator.calcValueY);
                break;
        }

        calculator.calcValueX = calcResult;
        calculator.calcValueY = null;
        updateDisplay(calcResult);
    }

    var buttonsNumber = document.getElementsByClassName('number');
    for (var i = 0; i < buttonsNumber.length; i++) {
        buttonsNumber[i].addEventListener('click', function (event) {
            var numberValue = event.target.textContent;
            numberClick(numberValue);
        });
    }

    var buttonsOperation = document.getElementsByClassName('operation');
    for (var j = 0; j < buttonsOperation.length; j++) {
        buttonsOperation[j].addEventListener('click', function (event) {
            var operation = event.target.getAttribute('data-operation');
            operationClick(operation);
        });
    }
})();